DESCRIPTION = "GStreamer Qt bindings"
SECTION = "graphics"
LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://COPYING;md5=2d5025d4aa3495befef8f17206a5b0a1"
PV = "0.10.3.1"

DEPENDS = "boost qtbase qtdeclarative"

SRCREV = "10798183484bc2ae7496db0c1193109d1702d4b5"
SRC_URI = "git://anongit.freedesktop.org/gstreamer/qt-gstreamer"

S = "${WORKDIR}/git"

EXTRA_OECMAKE = " -DQT_VERSION=5 -DOE_QMAKE_PATH_EXTERNAL_HOST_BINS=/home/raimo/yocto/build/tmp/sysroots/i686-linux/usr/bin/qt5"

FILES_${PN} += "${libdir}/gstreamer-0.10/*.so ${datadir}"

FILES_${PN}-dbg += "${libdir}/gstreamer-0.10/.debug"
FILES_${PN}-dev += "${libdir}/gstreamer-0.10/*.la ${libdir}/pkgconfig/*.pc"
FILES_${PN}-dev += "${libdir}/cmake/*"
FILES_${PN}-staticdev += "${libdir}/gstreamer-0.10/*.a"

INSANE_SKIP_${PN} = "installed-vs-shipped"
inherit pkgconfig cmake 


