include recipes-core/images/core-image-base.bb
inherit core-image

IMAGE_FEATURES = "splash tools-debug debug-tweaks ssh-server-openssh "

DISTRO_FEATURES_append = " pulseaudio"
DISTRO_FEATURES_remove = "x11 wayland"
IMAGE_FEATURES_remove = "x11 x11-base"

IMAGE_INSTALL += " \
    dbus \
    packagegroup-fsl-gstreamer \
    packagegroup-fsl-tools-testapps \
    packagegroup-fsl-tools-benchmark \
    qtbase qtbase-plugins qtbase-fonts qtbase-examples \
    qtdeclarative qtdeclarative-examples qtmultimedia qtquick1 qtquickcontrols-qmlplugins \
    qtdeclarative-qmlplugins qtgraphicaleffects-qmlplugins qtmultimedia-qmlplugins \
    qtgstreamer \
    gdbserver \
    libcec \
    libexif \
"

export IMAGE_BASENAME = "fsl-image-raimo"
